package RPG.states;

import java.awt.Graphics;

import RPG.Game;
import RPG.Handler;
import RPG.entities.creatures.Enemies;
import RPG.entities.creatures.Player;
import RPG.worlds.World;



public class GameState extends State {
	
	private Player player;
	private Enemies enemies;
	private World world;
	
	public GameState(Handler handler){
		super(handler);
		world = new World(handler, "res/world/world1.txt");
		handler.setWorld(world);
		player = new Player(handler, 100, 100);
		enemies = new Enemies(handler, 100, 100);
	}
	
	@Override
	public void tick() {
		world.tick();
		player.tick();
	}

	@Override
	public void render(Graphics g) {
		world.render(g);
		player.render(g);
		enemies.render(g);
		g.drawString(player.getHealth() + "/10", 0, 0);
	}

}
