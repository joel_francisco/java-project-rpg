package RPG.states;

import java.awt.Graphics;

import RPG.Game;
import RPG.Handler;
import RPG.gfx.Assets;
import RPG.ui.ClickListener;
import RPG.ui.UIImageButton;
import RPG.ui.UIManager;

public class MenuState extends State {

	private UIManager uiManager;

	public MenuState(Handler handler) {
		super(handler);
		uiManager = new UIManager(handler);
		handler.getMouseManager().setUIManager(uiManager);

		uiManager.addObject(new UIImageButton(175, 100, 128, 64, Assets.boutton_start, new ClickListener(){
			@Override
			public void onClick() {
				handler.getMouseManager().setUIManager(null);
				State.setState(handler.getGame().gameState);
			}
		}));
		
		uiManager.addObject(new UIImageButton(175, 400, 128, 64, Assets.boutton_exit, new ClickListener(){
			@Override
			public void onClick() {
				System.exit(0);
			}
		}));
		
		uiManager.addObject(new UIImageButton(175, 250, 128, 64, Assets.boutton_multi, new ClickListener(){
			@Override
			public void onClick() {
				System.out.println("No multiplayer sorry");
			}
		}));
	}

	@Override
	public void tick() {
		uiManager.tick();
	}

	@Override
	public void render(Graphics g) {
		uiManager.render(g);
	}

}
