package RPG.gfx;

import java.awt.image.BufferedImage;

public class Assets {
	
	private static final int height = 48, width = 48;
	private static final int height1 = 25, width1 = 25;
	
	
	public static BufferedImage dirt, grass, rock, idle, enemies;
	public static BufferedImage[] player_down, player_up, player_left, player_right;
	public static BufferedImage[] boutton_start, boutton_multi, boutton_exit;
	
	public static void init() {
		
		SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("res/texture/Outside_A2.png"));
		SpriteSheet sheetPerso = new SpriteSheet(ImageLoader.loadImage("res/texture/Actor2.png"));
		SpriteSheet sheetMenu = new SpriteSheet(ImageLoader.loadImage("res/texture/boutons-cartoony-pour-creation-jeux-video_90608-61.jpg"));
		SpriteSheet sheetEnemies = new SpriteSheet(ImageLoader.loadImage("res/texture/Monster.png"));
		
		enemies = sheetEnemies.crop(width * 4,0 , width, height);
		
		
		boutton_start = new BufferedImage[2];
		boutton_exit = new BufferedImage[2];
		boutton_multi = new BufferedImage[2];
		boutton_start[0] = sheetMenu.crop(0, 0, width1, height1);
		boutton_start[1] = sheetMenu.crop(width1 * 3 , 0, width1, height1);
		boutton_exit[0] = sheetMenu.crop(width1 * 2, 0, width1, height1);
		boutton_exit[1] = sheetMenu.crop(width1 * 5, 0, width1, height1);
		boutton_multi[0] = sheetMenu.crop(width1 * 1, 0, width1, height1);
		boutton_multi[1] = sheetMenu.crop(width1 * 4, 0, width1, height1);
		
		
		idle = sheetPerso.crop(0, height * 4, width, height);
		
		player_down = new BufferedImage[3];
		player_up = new BufferedImage[3];
		player_left = new BufferedImage[3];
		player_right = new BufferedImage[3];
		
		player_down[0] = sheetPerso.crop(0, height * 4, width, height);
		player_down[1] = sheetPerso.crop(width * 1, height * 4, width, height);
		player_down[2] = sheetPerso.crop(width * 2, height * 4, width, height);
		player_up[0] = sheetPerso.crop(0, height * 7, width, height);
		player_up[1] = sheetPerso.crop(width * 1, height * 7, width, height);
		player_up[2] = sheetPerso.crop(width * 2, height * 7, width, height);
		player_left[0] = sheetPerso.crop(0, height * 5, width, height);
		player_left[1] = sheetPerso.crop(width * 1, height * 5, width, height);
		player_left[2] = sheetPerso.crop(width * 2, height * 5, width, height);
		player_right[0] = sheetPerso.crop(0, height * 6, width, height);
		player_right[1] = sheetPerso.crop(width * 1, height * 6, width, height);
		player_right[2] = sheetPerso.crop(width * 2, height * 6, width, height);
		
		
		dirt = sheet.crop(0, height * 4, width, height);
		grass = sheet.crop(width * 5, height * 9, width, height);
		rock = sheet.crop(width * 12, height * 9, width, height);
		
	}

}
